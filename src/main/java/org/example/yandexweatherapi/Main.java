package org.example.yandexweatherapi;

import com.google.gson.Gson;
import okhttp3.Response;
import org.example.yandexweatherapi.model.Forecast;
import org.testng.Assert;

import java.io.IOException;


public class Main {
    public static void getWeatherInfo(String lat, String lon, String lan, String limit, String hours, String extra) throws IOException {
        OkHttpBuilder okHttpBuilder = new OkHttpBuilder();
        Response response = okHttpBuilder.getResponse(lat, lon, lan, limit, hours, extra);
        Assert.assertEquals(response.code(), 200, "Wrong Response code");
        Assert.assertNotNull(response.body(), "Response has no body. Try another Request");
        String jsonText = response.body().string();
        Gson gson = new Gson();
        Forecast forecast = gson.fromJson(jsonText, Forecast.class);
        Assert.assertEquals(forecast.getInfo().getLat(), lat, "Info from Response doesn't match with your Request because of wrong Latitude");
        Assert.assertEquals(forecast.getInfo().getLon(), lon, "Info from Response doesn't match with your Request because of wrong Longitude");
        printOut(forecast);
    }

    private static void printOut(Forecast forecast) {
        if (forecast.getFact().getCondition().contains("rain")) {
            System.out.println("Current weather in " + forecast.getGeoObject().getLocality().getName() + ":\n"
                    + "Date: " + forecast.getDate().substring(0, 10) + "\n"
                    + "Temperature (C): " + forecast.getFact().getTemp() + "\n"
                    + "Condition: " + forecast.getFact().getCondition() + "\n"
                    + "Pressure (mm): " + forecast.getFact().getPressure() + "\n"
                    + "Humidity (%): " + forecast.getFact().getHumidity() + "\n"
                    + "Wind Speed (m/s): " + forecast.getFact().getWindSpeed() + "\n");
        } else {
            System.out.println("At this location no rain!");
        }
    }

    public static void main(String[] args) throws IOException {
        Object[][] testData = TestData.getWeather();
        for (Object[] testDatum : testData) {
            getWeatherInfo((String) testDatum[0], (String) testDatum[1], (String) testDatum[2], (String) testDatum[3],
                    (String) testDatum[4], (String) testDatum[5]);
        }
    }
}